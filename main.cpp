#include "Module.h"

/**
Task: Implement file searching by their content by given path on filesystem
Requirements:
    -- number of threads from argument list
    -- synchronise threads
    -- should output file and his path if there were found appropriate content
Format output:
 Found [n] files
 ----------------------------------
 | file.txt | /home/ivan/projects |
 | file.cpp | /home/ivan/projects |
 ---------------------------------
 */



int main(int argc, char* argv[]) {

    initProgramOptions(argc, argv);
    if(gl::vm.count("help")){
        std::cout << gl::opts << '\n';
        return 0;
    }
    if(!gl::vm.count("content")){
        std::cout << "BAD USAGE\n";
        std::cout << gl::vm["help"].as<std::string>() << '\n';
        return 0;
    }

    // init data from arguments
    gl::content = gl::vm["content"].as<std::string>();
    std::string dirPath = ".";
    int threadsNum = 1;
    if(gl::vm.count("path")){
        dirPath = gl::vm["path"].as<std::string>();
    }
    if(gl::vm.count("threads")){
        threadsNum = std::stoi(gl::vm["threads"].as<std::string>());
    }


    // init other data
    gl::dirIterator = std::filesystem::recursive_directory_iterator(dirPath);
    gl::threads.reserve(threadsNum + 1);

    // initializing threads
    gl::dirIterMutex.lock();
    for(int th_num = 0; gl::dirIterator != std::filesystem::end(gl::dirIterator) && th_num < threadsNum; ++gl::dirIterator, ++th_num){
        gl::threads.emplace_back(std::thread(worker));
    }
    gl::dirIterMutex.unlock();

    // waiting for threads to finish their work ...
    for(auto& thread: gl::threads){
        thread.join();
    }

    outputFoundFiles();
}

