#include "../Module.h"
#include <gtest/gtest.h>

TEST(HelloWorldTest, HelloWorld) {
    ASSERT_EQ("Hello world", getHelloWorld());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}