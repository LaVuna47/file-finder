FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y libboost-dev libboost-all-dev libboost-program-options-dev cmake g++ \
                libgtest-dev

WORKDIR /usr/src/file-finder

COPY . /usr/src/file-finder
RUN cmake . && make


CMD ["./file-finder","--content=#include", "--threads=3"]