//
// Created by ivan on 29.05.21.
//

#ifndef FILEFINDER_MODULE_H
#define FILEFINDER_MODULE_H

// std
#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <filesystem>
#include <fstream>
#include <iomanip>
// boost
#include <boost/program_options.hpp>

namespace gl {
    // threads
    std::vector<std::thread> threads;
    // content to be searched
    std::string content;

    // shared data
    std::filesystem::recursive_directory_iterator dirIterator;

    // files that were found
    std::vector<std::string> foundFiles;

    // objects to synchronize threads
    std::mutex dirIterMutex;
    std::mutex foundFilesMutex;

    // program options
    boost::program_options::variables_map vm;
    boost::program_options::options_description opts("file-finder options");

}
/*
  Returns empty string if there aren't any task left.
  Returns path for file to get job done.
 */
std::string getTask() {
    std::lock_guard<std::mutex> lock(gl::dirIterMutex);

    // iterating to find actual file (not directory)
    while(true) {
        if(gl::dirIterator == std::filesystem::end(gl::dirIterator))
            break;
        if(!gl::dirIterator->is_regular_file())
            ++gl::dirIterator;
        else
            break;
    }

    // checking if gl::dirIterator reached his end
    if(gl::dirIterator == std::filesystem::end(gl::dirIterator)){
        return {};
    }
    else {
        const std::string &path = gl::dirIterator->path();
        ++gl::dirIterator;
        return path;
    }
}

/*
 Performs content searching job on a given file path.
 */
void worker(){
    std::fstream stream;
    bool fileIsFound;
    while(true) {
        fileIsFound = false;
        std::string task = getTask();
        if (task.empty()) {
            return;
        }
        // work ...
        stream.open(task, std::ios::in);

        if(!stream.is_open()){
            std::cerr << "Failed to open " << task << '\n';
            continue;
        }

        std::string line;
        while(getline(stream,line)){
            /// perform some search
            if(line.find(gl::content) != std::string::npos) {
                fileIsFound = true;
                break;
            }
        }

        if(fileIsFound){
            std::lock_guard<std::mutex> lock(gl::foundFilesMutex);
            gl::foundFiles.emplace_back(task);
        }

        stream.close();
    }
}

/*
Outputs found files in formatted way.
 */
void outputFoundFiles(){
    uint32_t dirPathWidth = 1;
    uint32_t filenameWidth = 1;
    uint32_t pos;

    // setting Widths to output in nice way
    for(auto & file: gl::foundFiles){
        pos = file.find_last_of('/');
        if(pos == std::string::npos){
            // some error handling
            continue;
        }
        dirPathWidth = std::max(dirPathWidth, pos + 1);
        filenameWidth = std::max(filenameWidth, (uint32_t)file.size() - pos + 1);
    }
    std::string minus(dirPathWidth + filenameWidth + 7,'-');
    std::cout << minus << '\n';
    std::cout << "| " << std::setw((int)dirPathWidth) << "DIRECTORY PATH" \
            << " | " << std::setw((int)filenameWidth) << "FILENAME" << " |\n";
    std::cout << minus << '\n';

    for(auto & file: gl::foundFiles){
        pos = file.find_last_of('/');
        if(pos == std::string::npos){
            // some error handling
            continue;
        }
        std::cout << "| " << std::setw((int)dirPathWidth) << file.substr(0,pos + 1) \
            << " | " << std::setw((int)filenameWidth) << file.substr(pos) << " |\n";
    }

    std::cout << minus << '\n';
    std::cout << "Found " << gl::foundFiles.size() << " files with content \"" << gl::content << "\".\n";
}

/*
Initialises program options
 */
void initProgramOptions(int argc, char* argv[]){
    gl::opts.add_options()
            ("help", "Specify --content=[content to search] --threads=[number of threads](optional) --path=[directory path](optional)\n"
                     "directory path by default is current directory. Number of threads is 1 y default")
            ("path", boost::program_options::value<std::string>(), "to specify a path, cur directory by default")
            ("content", boost::program_options::value<std::string>(), "to specify a content to search")
            ("threads", boost::program_options::value<std::string>(), "to specify number of threads to search a content");

    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, gl::opts), gl::vm);
}

std::string getHelloWorld(){
    return "Hello world";
}
#endif //FILEFINDER_MODULE_H
