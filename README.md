***File Finder project***


Requirement :
> cmake(v 3.16), boost, google test(for testing)

> Get project
>> git clone https://gitlab.com/LaVuna47/file-finder.git
>
> build project
>> cd file-finder
> 
>> mkdir build && cd build && cmake .. && make
> 
> install 
> 
>> cmake install ..
> 
>> make install
> 
> run
>
>> ./file-finder --path=.. --content="#include" --threads=4
>
> Or if installed
> 
>> file-finder --path=.. --content="#include" --threads=4
